fun main() {

 val firstCar = BmwX6()
 firstCar.aboutCar()

 println("___________________________________")
 println()

 val secondCar = ToyotaPrius()
 secondCar.aboutCar()

 println("___________________________________")
 println()

 val thirdCar = Tesla()
 thirdCar.aboutCar()
}