class BmwX6 : Car {
    override fun showMaxSpeed() = 250

    override fun colorOfCar() = "Black"
    override fun numberOfDoors() = 5

    override var name = "Bmw X6"

    val fuel = Car.typeOfFuel.Diesel
    val engine = Car.typeOfEngine.Petrol

    fun aboutCar() {
        println(name)
        println("Color: ${colorOfCar()}")
        println("Max speed: ${showMaxSpeed()}")
        println("Doors: ${numberOfDoors()}")
        println("Fuel: $fuel")
        println("Engine: $engine")
    }
}