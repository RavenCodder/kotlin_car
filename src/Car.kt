interface Car {

    fun showMaxSpeed() : Int
    fun colorOfCar() : String
    fun numberOfDoors() : Int
    var name: String

    enum class typeOfFuel(val fuel: String) {
        Electrical("Electrical"),
        Fuel("Fuel"),
        Diesel("Diesel")
    }

    enum class typeOfEngine(engine: String) {
        Hybrid("Hybrid"),
        Electric("Electric"),
        Petrol("Petrol")
    }
}