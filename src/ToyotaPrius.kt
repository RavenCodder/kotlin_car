class ToyotaPrius : Car {
    override fun showMaxSpeed() = 180

    override fun colorOfCar() = "White"
    override fun numberOfDoors() = 5

    override var name = "Toyota Prius"
    val fuel = Car.typeOfFuel.Fuel
    val engine = Car.typeOfEngine.Hybrid

    fun aboutCar() {
        println(name)
        println("Color: ${colorOfCar()}")
        println("Max speed: ${showMaxSpeed()}")
        println("Doors: ${numberOfDoors()}")
        println("Fuel: $fuel")
        println("Engine: $engine")
    }
    }
