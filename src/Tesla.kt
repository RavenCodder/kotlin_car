class Tesla: Car {
    override fun showMaxSpeed() = 300

    override fun colorOfCar() = "Silver"
    override fun numberOfDoors() = 3

    override var name = "Tesla S"
    val fuel = Car.typeOfFuel.Electrical
    val engine = Car.typeOfEngine.Electric

    fun aboutCar() {
        println(name)
        println("Color: ${colorOfCar()}")
        println("Max speed: ${showMaxSpeed()}")
        println("Doors: ${numberOfDoors()}")
        println("Fuel: $fuel")
        println("Engine: $engine")
    }

}